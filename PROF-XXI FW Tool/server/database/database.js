const mysql = require("mysql");

const db = mysql.createConnection({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
});

db.connect(function (err) {
  if (err) {
    console.log(err.message);
  } else {
    console.log("Successfully connected to the database on AWS RDS");
  }
});

module.exports = db;
