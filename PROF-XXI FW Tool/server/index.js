require("dotenv").config({ path: "./config.env" });
const express = require("express");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors());

const userRoute = require("./routes/userRoute");
app.use("/api/user", userRoute);

const unitRoute = require("./routes/unitRoute");
app.use("/api/units", unitRoute);

const linkRoute = require("./routes/linkRoute");
app.use("/api/links", linkRoute);

const dashboardRoute = require("./routes/dashboardRoute");
app.use("/api/dashboards", dashboardRoute);

app.listen(3001, () => {
  console.log("runing server on port 3001");
});
