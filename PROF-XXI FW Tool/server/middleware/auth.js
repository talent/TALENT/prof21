const jwt = require("jsonwebtoken");

const auth = async (req, res, next) => {
  try {
    //console.log(req.headers.authorization);
    const token = req.headers.authorization.split(" ")[1];

    // Other Tokens Like Google ones have a length > 500 ( Used in case of setting up another authentification system )
    const isCustomAuth = token.length < 500;

    let decodedData;

    if (token && isCustomAuth) {
      decodedData = jwt.verify(token, process.env.JWT_SECRET);

      req.userId = decodedData.id;
      req.userEmail = decodedData.email;
      //console.log(decodedData.email + " Connected !");
    }

    next();
  } catch (err) {
    console.log(err);
  }
};

module.exports = auth;
