import React, { useState, useEffect } from "react";
import Axios from "axios";
import axios from "axios";

import "./Default.css";
import { Link, Redirect, useHistory } from "react-router-dom";
import Footer from "../../components/Footer";

function Registration() {
  const [gdpr, setGdpr] = useState(false);
  const [registrationState, setregistrationState] = useState("");
  const history = useHistory();
  const [user, setUser] = useState({
    organizationName: "",
    universityWebsite: "",
    firstName: "",
    lastName: "",
    position: "Teacher",
    profile: "",
    email: "",
    password: "",
  });

  Axios.defaults.withCredentials = true;

  const register = (e) => {
    e.preventDefault();
    if (user.email && user.password && gdpr) {
      Axios.post("/api/user/register", {
        user: user,
      }).then((response) => {
        setregistrationState(response.data.message);
      });
      setUser({
        organizationName: "",
        universityWebsite: "",
        firstName: "",
        lastName: "",
        position: "Teacher",
        profile: "",
        email: "",
        password: "",
      });
    } else {
      console.log("You need to accept the terms of GDPR");
    }
  };

  const handleChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUser({ ...user, [fieldName]: fieldValue });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      gdpr &&
      user.organizationName &&
      user.firstName &&
      user.email &&
      user.password
    ) {
      console.log("User created! ");

      setUser({
        organizationName: "",
        universityWebsite: "",
        firstName: "",
        lastName: "",
        position: "",
        profile: "",
        email: "",
        password: "",
      });
    }
  };

  useEffect(() => {
    document.title = "Registration";
  }, []);

  return (
    <>
      <h2> Registration Form</h2>
      <div className="middle-container">
        <article className="form">
          <form>
            <div>
              <label htmlFor="organizationName">Name of organization : </label>
              <input
                type="text"
                id="organizationName"
                name="organizationName"
                value={user.organizationName}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="universityWebsite">
                Your university's official website :
              </label>
              <input
                type="text"
                id="universityWebsite"
                name="universityWebsite"
                value={user.universityWebsite}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="firstName">First Name :</label>
              <input
                type="text"
                id="firstName"
                name="firstName"
                value={user.firstName}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="lastName">Last Name :</label>
              <input
                type="text"
                id="lastName"
                name="lastName"
                value={user.lastName}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="position">Position :</label>
              <select
                name="position"
                id="position"
                value={user.position}
                onChange={handleChange}
              >
                <option value="Teacher">Teacher</option>
                <option value="Administrator"> Administrator</option>
                <option value="Manager"> Manager</option>
              </select>
            </div>

            <div>
              <label htmlFor="profile">Your profile :</label>
              <input
                type="text"
                id="profile"
                name="profile"
                value={user.profile}
                onChange={handleChange}
              />
              <p style={{ color: "blue" }}>
                Examples : LinkedIn / researchGate / University profile
              </p>
            </div>

            <div>
              <label htmlFor="email">Your email :</label>
              <input
                type="email"
                id="email"
                name="email"
                value={user.email}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="password">Your password :</label>
              <input
                type="password"
                id="password"
                name="password"
                value={user.password}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="gdpr"> </label>
              <input
                name="gdpr"
                type="checkbox"
                defaultChecked={gdpr}
                onChange={() => setGdpr(!gdpr)}
              ></input>
              <a href="https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679">
                <span style={{ color: "red" }}>
                  General Data Protection Regulation
                </span>
              </a>
            </div>

            <button type="submit" className="btn" onClick={register}>
              Send
            </button>
            <h4 style={{ color: "red", marginTop: "2rem" }}>
              {registrationState}
            </h4>
          </form>
        </article>

        <img
          style={{ marginLeft: "10rem" }}
          width="50%"
          height="50%"
          src="images/design2.jpg"
          alt=""
        />
      </div>
      <Footer />
    </>
  );
}

export default Registration;
