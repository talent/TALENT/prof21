import React, { useState, useEffect } from "react";
import Axios from "axios";
import "../../Default.css";
import Footer from "../../../Footer";

import RegistrationForm from "./RegistrationForm";
import validateRegistration from "./validateRegistration";

function Registration() {
  const [gdpr, setGdpr] = useState(false);
  const [registrationState, setregistrationState] = useState("");

  Axios.defaults.withCredentials = true;

  const register = () => {
    if (gdpr) {
      Axios.post("/api/user/register", {
        user: user,
      }).then((response) => {
        setregistrationState(response.data.message);
      });
    } else {
      console.log("You need to accept the terms of GDPR");
    }
  };
  const { handleChange, handleSubmit, user, errors } = RegistrationForm(
    register,
    validateRegistration
  );

  useEffect(() => {
    document.title = "Registration";
  }, []);

  return (
    <>
      <h2> Registration Form</h2>
      <div className="middle-container">
        <article className="form">
          <form>
            <div>
              <label htmlFor="organizationName">Name of organization : </label>
              <input
                type="text"
                id="organizationName"
                name="organizationName"
                value={user.organizationName}
                onChange={handleChange}
              />
            </div>

            <h5 className="warning">{errors.organizationName}</h5>

            <div>
              <label htmlFor="universityWebsite">
                Your university's official website :
              </label>
              <input
                type="text"
                id="universityWebsite"
                name="universityWebsite"
                value={user.universityWebsite}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="firstName">First Name :</label>
              <input
                type="text"
                id="firstName"
                name="firstName"
                value={user.firstName}
                onChange={handleChange}
              />
            </div>
            <h5 className="warning"> {errors.firstName}</h5>

            <div>
              <label htmlFor="lastName">Last Name :</label>
              <input
                type="text"
                id="lastName"
                name="lastName"
                value={user.lastName}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="position">Position :</label>
              <select
                name="position"
                id="position"
                value={user.position}
                onChange={handleChange}
              >
                <option value="Teacher">Teacher</option>
                <option value="Administrator"> Administrator</option>
                <option value="Manager"> Manager</option>
              </select>
            </div>

            <div>
              <label htmlFor="profile">Your profile :</label>
              <input
                type="text"
                id="profile"
                name="profile"
                value={user.profile}
                onChange={handleChange}
              />
              <p style={{ color: "blue" }}>
                Examples : LinkedIn / researchGate / University profile
              </p>
            </div>

            <div>
              <label htmlFor="email">Your email :</label>
              <input
                type="email"
                id="email"
                name="email"
                value={user.email}
                onChange={handleChange}
              />
            </div>
            <h5 className="warning">{errors.email}</h5>

            <div>
              <label htmlFor="password">Your password :</label>
              <input
                type="password"
                id="password"
                name="password"
                value={user.password}
                onChange={handleChange}
              />
            </div>
            <h5 className="warning"> {errors.password}</h5>

            <div>
              <label htmlFor="gdpr"> </label>
              <input
                name="gdpr"
                type="checkbox"
                defaultChecked={gdpr}
                onChange={() => setGdpr(!gdpr)}
              ></input>
              <a href="https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679">
                <span style={{ color: "red" }}>
                  General Data Protection Regulation
                </span>
              </a>
            </div>

            <button type="submit" className="btn" onClick={handleSubmit}>
              Send
            </button>
            <h4 style={{ color: "red", marginTop: "2rem" }}>
              {registrationState}
            </h4>
          </form>
        </article>

        <img
          style={{ marginLeft: "10rem" }}
          width="50%"
          height="50%"
          src="images/design2.jpg"
          alt=""
        />
      </div>
      <Footer />
    </>
  );
}

export default Registration;
