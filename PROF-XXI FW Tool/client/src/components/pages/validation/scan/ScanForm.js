import { useState, useEffect } from "react";
import validateScan from "./validateScan";

const ScanForm = (Submit) => {
  const [userAnswers, setUsersAnswers] = useState({
    A1: "",
    A2: "",
    A3: "",
    A4: "",
    A5: "",
    A6: "",
    A7: "",
    A8: "",
    A9: "",
    A10: "",
    A11: "",
    B1: "",
    B2: "",
    B3: "",
    B4: "",
    B5: "",
    B6: "",
    B7: "",
    B8: "",
    B9: "",
    B10: "",
    B11: "",
    C1: "",
    C2: "",
    C3: "",
    C4: "",
    C5: "",
    C6: "",
    C7: "",
    C8: "",
    C9: "",
    C10: "",
    C11: "",
    D1: "",
    D2: "",
    D3: "",
    D4: "",
    D5: "",
    D6: "",
    D7: "",
    D8: "",
    D9: "",
    E1: "",
    E2: "",
    E3: "",
    E4: "",
    E5: "",
    E6: "",
    E7: "",
    E8: "",
  });

  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    position: "Teacher",
  });

  const [isSubmitting, setIsSubmitting] = useState(false);

  const [errors, setErrors] = useState({});

  const handleChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUsersAnswers({ ...userAnswers, [fieldName]: fieldValue });
  };

  const handleUserChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUser({ ...user, [fieldName]: fieldValue });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setErrors(validateScan(user, userAnswers));
    setIsSubmitting(true);
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      Submit();
    }
  }, [errors]);

  return {
    handleChange,
    handleUserChange,
    handleSubmit,
    user,
    userAnswers,
    errors,
  };
};

export default ScanForm;
