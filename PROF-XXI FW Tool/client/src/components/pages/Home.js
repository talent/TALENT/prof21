import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import FrontText from "../../components/FrontText";
import ReactPlayer from "react-player";
import Footer from "../../components/Footer";
import "./Home.css";

function Home() {
  useEffect(() => {
    document.title = "Home";
  }, []);

  return (
    <>
      {/*<div className="buttons-container">
        <div className="buttons-menu">
          <Link to="/login">
            <button to="/login" className="btn">
              Log In
            </button>
          </Link>

          <Link to="/about">
            <button className="btn"> About Us</button>
          </Link>

          <Link to="/contact">
            <button className="btn">Contact Us</button>
          </Link>
        </div>
  </div> */}

      <div className="middle-container-home">
        <div className="element-container-home">
          <ReactPlayer
            className="element"
            height="500px"
            width="800px"
            url="https://www.youtube.com/watch?v=rz9RQmqWgvM"
          />
        </div>
        <div className="element-container">
          <p className="element-text">
            The Framework Prof-XXI was created to guide HEIs in the design and
            implementation of TLCs for the 21st century. The Prof-XXI framework
            proposes a set of competences that HEIs can take as a reference to
            develop the actions and strategies of their TLCs. The Prof-XXI
            framework organizes these competences into 5 levels and 5 dimensions
            that interrelate to define a total of 50 competences.
          </p>
        </div>
      </div>

      <div className="logos-container">
        <a href="http://www.profxxi.org/?lang=en">
          <img src="/images/univscollab.jpg" alt="" />
        </a>
      </div>

      <Footer />
    </>
  );
}

export default Home;
