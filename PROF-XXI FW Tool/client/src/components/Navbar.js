import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import "./Navbar.css";
import decode from "jwt-decode";

function Navbar() {
  const [button, setButton] = useState(true);
  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );

  const location = useLocation();

  const handleLogOut = () => {
    localStorage.removeItem("profile");
    window.location = "/login";
  };

  useEffect(() => {
    const token = profile?.token;

    if (token) {
      const decodedToken = decode(token);

      setTimeout(() => {
        handleLogOut();
      }, 1000 * 60 * 60);

      if (decodedToken.exp * 1000 < new Date().getTime()) {
        handleLogOut();
      }
    }

    JSON.parse(localStorage.getItem("profile"));
  }, [location]);

  return (
    <>
      <nav className="navbar">
        <div className="navbar-container">
          <Link to="/" className="navbar-logo">
            <img width="70" height="40" src="/images/profxxi.png" alt="" />
          </Link>

          <ul className="navbar-menu">
            <li className="navbar-item">
              <Link to="/" className="navbar-links">
                Home
              </Link>
            </li>

            <li className="navbar-item">
              <Link to="/about" className="navbar-links">
                About Us
              </Link>
            </li>

            <li className="navbar-item">
              <Link to="/register" className="navbar-links">
                Register
              </Link>
            </li>

            {!profile && (
              <li className="navbar-item">
                <Link to="/login" className="navbar-links">
                  Login
                </Link>
              </li>
            )}

            {profile && (
              <>
                <li className="navbar-item">
                  <Link to="/workingarea" className="navbar-links">
                    Working Area
                  </Link>
                </li>

                <li className="navbar-item">
                  <button className="navbar-button" onClick={handleLogOut}>
                    Logout
                  </button>
                </li>
              </>
            )}
          </ul>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
